#ifndef OOP_AUTOMATA_SRC_NON_TERMINAL_H_
#define OOP_AUTOMATA_SRC_NON_TERMINAL_H_

#include <string>
#include <math.h>

#include "GrammarSymbol.h"

class NonTerminal : public GrammarSymbol {
  using string = std::string;
  using us = unsigned short;

 public:
  static const us GenerateNumber(const char* arr, const us& len) {
    us num = 0;
    bool is_reading = false;
    bool has_been_read = false;
    us i = 1;
    if (i < len && arr[i++] == '(') {
      for (i; i < len && !has_been_read; i++) {
        const char& c = arr[i];
        if ('0' <= c && c <= '9') {
          is_reading = true;
          if (!has_been_read) {
            num *= 10;
            num += (c - '0');
          }
        } else {
          if (is_reading) has_been_read = true;
          is_reading = false;
        }
      }
    }

    return num;
  }

  static const us GenerateNumber(const string& s) {
    return GenerateNumber(s.c_str(), (us)s.length());
  }

  NonTerminal() : GrammarSymbol('S'), number(0) { char_len = 1; }

  NonTerminal(const char* c, const us& len = 1)
      : GrammarSymbol(c[0]), number(NonTerminal::GenerateNumber(c, len)) {
    CalculateLengthOfString();
  }

  NonTerminal(const string& non_terminal)
      : GrammarSymbol(non_terminal[0]),
        number(NonTerminal::GenerateNumber(non_terminal)) {
    CalculateLengthOfString();
  }

  NonTerminal(const NonTerminal& nt) : GrammarSymbol(nt.letter) {
    number = nt.number;
    char_len = nt.char_len;
  };

  virtual bool IsTerminal() const { return false; }
  virtual bool IsEpsilon() const { return false; }

  virtual string Serialize() const {
    string result = "";
    result += letter;
    if (number) result += '(' + std::to_string(number) + ')';
    return result;
  }

  bool operator<(const NonTerminal& b) const {
    return CompareAGreatherThanB(b, *this);
  }

  bool operator>(const NonTerminal& b) const {
    return CompareAGreatherThanB(*this, b);
  }

  bool operator==(const NonTerminal& b) const {
    return !operator<(b) && !operator>(b);
  }

  const us& GetNonTerminalStringLength() const { return char_len; }
  const us& GetNumber() const { return number; }
  void SetNumber(us num) {
    number = num;
    CalculateLengthOfString();
  }

 private:
  us number;
  us char_len = 0;

  void CalculateLengthOfString() {
    char_len = 1;
    if (number) {
      char_len += 3;  // (, ) and a digit from num
      char_len += log10(number);
    }
  }

  bool CompareAGreatherThanB(const NonTerminal& a, const NonTerminal& b) const {
    if (a.letter == b.letter) {
      return a.number > b.number;
    }
    return a.letter > b.letter;
  }
};

// Hash function created for UnorderedMap
namespace std {
template <>
struct hash<NonTerminal> {
  size_t operator()(const NonTerminal& k) const {
    // Compute individual hash values for two data members and combine them
    // using XOR and bit shifting
    return ((hash<char>()(k.GetLetter()) ^ (hash<unsigned short>()(k.GetNumber()) << 1)) >> 1);
  }
};
}  // namespace std

#endif  // OOP_AUTOMATA_SRC_NON_TERMINAL_H_
