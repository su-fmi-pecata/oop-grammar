#ifndef OOP_AUTOMATA_SRC_TERMINALS_TERMINAL_H_
#define OOP_AUTOMATA_SRC_TERMINALS_TERMINAL_H_

#include <string>

#include "GrammarSymbol.h"

class Terminal : public GrammarSymbol {
 public:
  Terminal(const char& letter) : GrammarSymbol(letter) { }
  Terminal(const Terminal& terminal_to_copy)
      : GrammarSymbol(terminal_to_copy) {}
  virtual bool IsTerminal() const { return true; }
  virtual bool IsEpsilon() const { return false; }
};

#endif  // OOP_AUTOMATA_SRC_TERMINALS_TERMINAL_H_
