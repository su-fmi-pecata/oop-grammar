#include "System.h"

#include <fstream>
#include <iostream>

#include "Grammar.h"

/*!
  @brief Adds rule to grammar

  @param (string&) input Recieves grammar's id and rule in
  format id_grammar id_rule
  @return void

  Parses rule and adds it to grammar.

  @author Petar Toshev
 */
void System::AddRule(string& input) {
  string id_grammar, rule;
  if (!SeparateInputViaFirstSpace(input, id_grammar, rule)) {
    std::cout << "Cannot parse id's\n";
    return;
  }

  Grammar& g = *GetGrammarById(id_grammar);
  if (&g == nullptr) {
    MissingGrammarWithId(id_grammar);
    return;
  }

  if (g.AddRule(rule)) std::cout << "Added successfully";
}

/*!
  @brief Moves grammar to Chomsky format.

  @param (string&) input grammar's id
  @return void

  Uses @ref Grammar::TransferToChomsky

  @author Petar Toshev
*/
void System::Chomskify(string& input) {
  Grammar& g = *GetGrammarById(input);
  if (&g == nullptr) {
    MissingGrammarWithId(input);
    return;
  }

  g.TransferToChomsky();
  std::cout << "Chomskified";
}

/*!
  @brief Check if grammar is in Chomsky format.

  @param (string&) input grammar's id
  @return void

  Uses @ref Grammar::IsInChomsky

  @author Petar Toshev
*/
void System::Chomsky(string& input) {
  Grammar& g = *GetGrammarById(input);
  if (&g == nullptr) {
    MissingGrammarWithId(input);
    return;
  }

  std::cout << "Grammar is ";
  if (!g.IsInChomsky()) {
    std::cout << "NOT ";
  }
  std::cout << "in Chomsky!";
}

/*!
  @brief Function for concatenating two grammars.

  @param (string&) input Recieves two grammars id's in format id1 id2
  @return void

  Uses @ref ConcatenateOrUnion

  @author Petar Toshev
*/
void System::Concat(string& input) {
  ConcatenateOrUnion(input, &System::ConcatenateTwoGrammars);
}

/*!
  @brief Function for copying grammar.

  @param (string&) input Id of grammar which to be copied
  @return void

  Creates copy of grammar.

  @author Petar Toshev
*/
void System::Copy(string& input) {
  Grammar& g = *GetGrammarById(input);
  if (&g == nullptr) {
    MissingGrammarWithId(input);
    return;
  }
  Grammar* g_new = new Grammar(g);
  grammars.insert({g_new->GetId(), g_new});
  std::cout << "Grammar has been copied sucessfully to id " << g_new->GetId();
}

/*!
  @brief Check if word is in grammar language.

  @param (string& input) Id of grammar and word, separated by single space
  @return void

  Uses @ref Grammar::CYK

  @author Petar Toshev
*/
void System::CYK(string& input) {
  string id_grammar, word;
  if (!SeparateInputViaFirstSpace(input, id_grammar, word)) {
    std::cout << "Cannot parse id's\n";
    return;
  }

  Grammar& g = *GetGrammarById(id_grammar);
  if (&g == nullptr) {
    MissingGrammarWithId(id_grammar);
    return;
  }

  if (!g.IsInChomsky()) {
    std::cout << "Grammar with id " << id_grammar << " is not in Chomsky";
    return;
  }

  bool contains = g.CYK(word);
  std::cout << "Word is " << (contains ? "" : "NOT ") << "part from grammar language";
}

/*!
  @brief Removes grammar from memory via id

  @param (string&) input Grammar's to be discarded id
  @return void

  Function for removing grammar from memory.

  @author Petar Toshev
*/
void System::Discard(string& input) {
  Grammar* g = GetGrammarById(input);
  if (g == nullptr) {
    MissingGrammarWithId(input);
    return;
  }
  grammars.erase(g->GetId());
  delete g;
  std::cout << "Grammar with id " << input << " has been removed successfully!";
}

/*!
  @brief Check if language of grammar is empty,

  @param (string&) input Grammar's id
  @return void

  @ref Grammar::IsEmpty

  @author Petar Toshev
*/
void System::Empty(string& input) {
  Grammar& grammar = *GetGrammarById(input);
  if (&grammar != nullptr)
    std::cout << "Language of grammar with id " << input << " is "
              << (grammar.IsEmpty() ? "" : "NOT ") << "empty";
  else
    MissingGrammarWithId(input);
}

/*!
  @brief Creates iter on grammar

  @param (string&) input Grammar's id
  @return void

  @ref Grammar::Iter

  @author Petar Toshev
*/
void System::Iter(string& input) {
  Grammar* g = GetGrammarById(input);
  if (g == nullptr) {
    MissingGrammarWithId(input);
    return;
  }
  if (!g->Iter())
    std::cout << "Cannot create";
  else
    std::cout << "Successfully created";
  std::cout << " iter on grammar with id " << input;
}

/*!
  @brief List all Grammar's ids

  @param input Doesn't matter
  @return void

  Function for listing all grammars in the system.

  @author Petar Toshev
*/
void System::List(string& input) {
  us number = 0;
  GrammarsMapType::iterator grammar = grammars.begin();
  while (grammar != grammars.end() && ++number)
    std::cout << ((grammar++)->first) << std::endl;
  std::cout << "Printed " << number << " grammar's ids\n";
}

/*!
  @brief Read grammar from file

  @param input file path
  @return void

  Creates Grammar in heap memory from file and add to list
  if parsed successfully.

  @author Petar Toshev
*/
void System::Open(string& input) {
  RemoveQuotesFormFilename(input);

  std::ifstream grammar_file;
  grammar_file.open(input);
  if (!grammar_file.is_open()) {
    std::cout << "Cannot open file: " << input;
    return;
  }

  Grammar* grammar = new Grammar(config, grammar_file);
  if (grammar->IsValid()) {
    grammars.emplace(grammar->GetId(), grammar);
    std::cout << "New grammar id: " << grammar->GetId();
  } else {
    std::cout << "Error parsing grammar...\n" << grammar->GetError();
    delete grammar;
  }
  grammar_file.close(); 
}

/*!
  @brief Prints grammar

  @param (string&) input Recieves grammar's id
  @return void

  Uses @ref Grammar::Print

  @author Petar Toshev
 */
void System::Print(string& input) {
  Grammar& grammar = *GetGrammarById(input);
  if (&grammar != nullptr)
    std::cout << "Printing grammar with id: " << input << std::endl
              << grammar.Print();
  else
    MissingGrammarWithId(input);
}

/*!
  @brief Removes rule from grammar

  @param (string&) input Recieves grammar's id and rule in
  format id_grammar id_rule
  @return void

  @author Petar Toshev
 */
void System::RemoveRule(string& input) {
  string id_grammar, id_rule;
  if (!SeparateInputViaFirstSpace(input, id_grammar, id_rule)) {
    std::cout << "Cannot parse id's\n";
    return;
  }

  Grammar& g = *GetGrammarById(id_grammar);
  if (&g == nullptr) {
    MissingGrammarWithId(id_grammar);
    return;
  }

  if (g.RemoveRule(std::stoi(id_rule)))
    std::cout << "Removed successfully";
  else
    std::cout << "Missing rule with id: " << id_rule
              << " in grammar with id: " << id_grammar;
}

/*!
  @brief Serializes grammar and saves it to file

  @param (string&) input Recieves grammar's id and file path
  in format grammar_id file_path
  @return void

  Uses @ref Grammar::Serialize
  and output saves to file

  @author Petar Toshev
 */
void System::Save(string& input) {
  string id_str, file;
  SeparateInputViaFirstSpace(input, id_str, file);
  RemoveQuotesFormFilename(file);

  std::ofstream fiile_to_save(file);
  if (!fiile_to_save.is_open()) {
    std::cout << "Unable to open file: " + file;
    return;
  }

  Grammar& grammar = *GetGrammarById(id_str);
  if (&grammar != nullptr) {
    fiile_to_save << grammar.Serialize();
    std::cout << "\nOK";
  } else
    MissingGrammarWithId(id_str);

  fiile_to_save.close();
}

/*!
  @brief Serializes grammar

  @param (string&) input Recieves grammar's id
  @return void

  Uses @ref Grammar::Serialize

  @author Petar Toshev
 */
void System::Serialize(string& input) {
  Grammar& grammar = *GetGrammarById(input);
  if (&grammar != nullptr)
    std::cout << grammar.Serialize();
  else
    MissingGrammarWithId(input);
}

#ifdef _DEBUG
void System::Test(string& input) {
  Serialize(input);
  Grammar& g = *GetGrammarById(input);
  Grammar* ng = new Grammar(g);
  grammars.emplace(ng->GetId(), ng);
  std::cout << "\n\n";
  ng->Serialize();
  std::cout << "\n\n";
  g.GET_TERMINALS()[0]->SET_LETTER('k');
  std::cout << "\n\n";
  Serialize(input);
  g.GET_NON_TERMINALS()[6]->SET_LETTER('T');
  g.GET_NON_TERMINALS()[6]->SetNumber(53);
  std::cout << "\n\n";
  Serialize(input);
}
#endif  // DEBUG

/*!
  @brief Function for union two grammars.

  @param input Recieves two grammars id's in format id1 id2
  @return void

  Uses @ref ConcatenateOrUnion

  @author Petar Toshev
*/
void System::Union(string& input) {
  ConcatenateOrUnion(input, &System::UnionTwoGrammars);
}