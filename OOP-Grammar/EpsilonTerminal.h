#ifndef OOP_AUTOMATA_SRC_EPSILON_TERMINAL_H_
#define OOP_AUTOMATA_SRC_EPSILON_TERMINAL_H_

#include "Terminal.h"

class EpsilonTerminal : public Terminal {
 public:
  EpsilonTerminal(const char& letter) : Terminal(letter) {}
  EpsilonTerminal(const EpsilonTerminal& terminal_to_copy)
      : Terminal(terminal_to_copy) {}

  virtual bool IsEpsilon() const { return true; }
};

#endif