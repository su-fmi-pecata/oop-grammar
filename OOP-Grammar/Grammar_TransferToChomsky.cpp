#include "Grammar.h"

#include <map>
#include <sstream>
#include <string>
#include <unordered_set>

/*!
  @brief Removes Long Rules from grammar

  @return void

  Creates map for transfering different sequences into different
  non terminals;
  Creates stringstream for adding separated rules;

  Uses @ref Rule::TransferToShort

  @author Petar Toshev
*/
void Grammar::RemoveLongRules() {
  map_for_transfering_to_chomsky_remove_long_rules_type remove_long_rules;
  string line;
  stringstream ss;

  // we have to use indexes, because of variable length of rules
  for (us i = 0; i < rules.size(); i++) {
    // reset string streams
    ss.str("");
    ss.clear();

    rules[i].TransferToShort(remove_long_rules, this, ss);
    while (std::getline(ss, line)) AddRule(line);
  }
}

/*!
  @brief Removes Epsilon Rules from grammar

  @return void

  @author Petar Toshev
*/
void Grammar::RemoveEpsilonRules() {
  unordered_set_for_epsilon_closing epsilon_non_terminals;
  // find set with all epsilon terminals
  {
    us epsilon_non_terminals_number = 700;  // random number, just have
    // to be != 0 to be able to start the algo
    while (epsilon_non_terminals_number != epsilon_non_terminals.size()) {
      // save before making changes
      epsilon_non_terminals_number = epsilon_non_terminals.size();

      for (Rule& rule : rules)
        rule.FindAllEpsilonTerminals(epsilon_non_terminals);
    }

    // check if start non terminal can be epsilon and add rule for that (just in
    // case we don't have one already
    if (epsilon_non_terminals.count(start_non_terminal) > 0)
      AddRule(start_non_terminal->Serialize() + " -> " + epsilon->Serialize());
  }

  // create all variants for removing epsilon non terminals
  for (Rule& rule : rules)
    rule.GenerateRulesWithRemovedEpsilonNonTerminal(epsilon_non_terminals,
                                                    start_non_terminal);
}

/*!
  @brief Removes Renaming Rules from grammar

  @return void

  Creates map with renaming pairs (@ref Rule::FindAllRenamingRules). Creates
  transitive close on map with renaming pairs. Removes all single non terminals
  (@ref Rule::RemoveRenamingRules). Swap map key-value NonTerminals because if
  A->C, C->BD => A->BD. Adds new rules via (@ref
  Rule::GenerateRulesWithRenamedNonTerminals).

  @author Petar Toshev
*/
void Grammar::RemoveRenamingRules() {
  map_for_transfering_to_chomsky_remove_renaming_rules_type
      renaming_non_terminals_map;

  // create map with renaming rules (like ~ relation)
  {
    // fill it for first time
    for (Rule& rule : rules)
      rule.FindAllRenamingRules(renaming_non_terminals_map);

    // extend map with transitive close (A -> B, B -> C => A -> C)
    {
      us renaming_non_terminals_map_size_previous = 7;  // still random number
      us renaming_non_terminals_map_size_current = 0;   // has to be 0
      while (renaming_non_terminals_map_size_previous !=
             renaming_non_terminals_map_size_current) {
        // save prev number
        renaming_non_terminals_map_size_previous =
            renaming_non_terminals_map_size_current;

        // reset current length
        renaming_non_terminals_map_size_current = 0;

        // extend
        map_for_transfering_to_chomsky_remove_renaming_rules_type::iterator it =
            renaming_non_terminals_map.begin();

        while (it != renaming_non_terminals_map.end()) {
          for (const NonTerminal* nt : it->second) {
            map_for_transfering_to_chomsky_remove_renaming_rules_type::iterator
                it_search = renaming_non_terminals_map.find(nt);
            if (it_search != renaming_non_terminals_map.end()) {
              for (const NonTerminal* nt : it_search->second) {
                if (nt != it->first) it->second.emplace(nt);
              }
            }
          }

          // calculate length
          renaming_non_terminals_map_size_current += it->second.size();

          it++;
        }
      }
    }
  }

  map_for_transfering_to_chomsky_remove_renaming_rules_type
      renaming_non_terminals_map_swapped;
  // swat keys with values
  {
    map_for_transfering_to_chomsky_remove_renaming_rules_type::iterator it =
        renaming_non_terminals_map.begin();

    while (it != renaming_non_terminals_map.end()) {
      const unordered_set_const_non_terminal_ptr& values_set = it->second;
      for (const NonTerminal* nt : values_set) {
        map_for_transfering_to_chomsky_remove_renaming_rules_type::iterator
            it_new_key = renaming_non_terminals_map_swapped.find(nt);
        if (it_new_key == renaming_non_terminals_map_swapped.end()) {
          renaming_non_terminals_map_swapped.emplace(
              nt, unordered_set_const_non_terminal_ptr());
          it_new_key = renaming_non_terminals_map_swapped.find(nt);
        }
        it_new_key->second.emplace(it->first);
      }

      it++;
    }
  }

  /*
  //  print
  if (true) {
    map_for_transfering_to_chomsky_remove_renaming_rules_type::iterator it =
        renaming_non_terminals_map.begin();
    while (it != renaming_non_terminals_map.end()) {
      std::cout << "NT: " << it->first->Serialize() << "\t|\t";
      for (const NonTerminal* nt : it->second)
        std::cout << nt->Serialize() << "\t";
      std::cout << "\n";
      it++;
    }
    std::cout << "\n\nNEW\n\n";
    it = renaming_non_terminals_map_swapped.begin();
    while (it != renaming_non_terminals_map_swapped.end()) {
      std::cout << "NT: " << it->first->Serialize() << "\t|\t";
      for (const NonTerminal* nt : it->second)
        std::cout << nt->Serialize() << "\t";
      std::cout << "\n";
      it++;
    }
  }
  */

  // remove single non terminal rules
  {
    for (us i = 0; i < rules.size(); i++)
      if (rules[i].RemoveRenamingRules()) {
        rules.erase(rules.begin() + i);
        i--;
      }
  }

  // create new rules (stringyfied)
  stringstream rules_to_add;
  for (Rule& rule : rules)
    rule.GenerateRulesWithRenamedNonTerminals(
        rules_to_add, renaming_non_terminals_map_swapped);
  string line;
  while (std::getline(rules_to_add, line)) AddRule(line);
}

/*!
  @brief Removes Long Combined from grammar

  @return void

  @ref Rule::TransferCombinedRules

  @author Petar Toshev
*/
void Grammar::RemoveCombinedRules() {
  map_for_transfering_to_chomsky_remove_combined_rules_type
      terminal_to_non_terminal;
  string line;
  stringstream ss;

  for (Rule& rule : rules)
    rule.TransferCombinedRules(terminal_to_non_terminal, this, ss);

  while (std::getline(ss, line)) AddRule(line);
}

/*!
  @brief Transfers grammar to Chomsky

  @return (void)

  Transfers grammar to Chomsky format using:
  @ref Grammar::RemoveLongRules
  @ref Grammar::RemoveEpsilonRules
  @ref Grammar::RemoveRenamingRules
  @ref Grammar::RemoveCombinedRules

  @author Petar Toshev
*/
void Grammar::TransferToChomsky() {
  //bool print = true;
  RemoveLongRules();
  //if (print) std::cout << "\n\n\nREMOVED LONG RULES\n" << Serialize();
  RemoveEpsilonRules();
  //if (print) std::cout << "\n\n\nREMOVED EPSILON RULES\n" << Serialize();
  RemoveRenamingRules();
  //if (print) std::cout << "\n\n\nREMOVED RENAMING RULES\n" << Serialize();
  RemoveCombinedRules();
}
