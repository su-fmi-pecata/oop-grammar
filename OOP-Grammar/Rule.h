/**
 * \class Rule
 *
 * This class is used to represent Grammar's rules like
 * A -> Asc|Bs|#
 *
 * \author Petar Toshev
 *
 */

#ifndef OOP_AUTOMATA_SRC_RULE_H_
#define OOP_AUTOMATA_SRC_RULE_H_

#include <map>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "Generic.h"
#include "NonTerminal.h"
#include "NonTerminalManager.h"
#include "Serializable.h"
#include "Terminal.h"
#include "Validate.h"

class Rule : public Validate, public Serializable {
  using string = std::string;
  using stringstream = std::stringstream;
  using us = unsigned short;

  using unordered_set_const_non_terminal_ptr =
      std::unordered_set<const NonTerminal*>;
  using const_non_terminal_ptr_pair =
      std::pair<const NonTerminal*, const NonTerminal*>;
  using CYK_vector_of_terminal_indexed = std::vector<us>;

  using changing_old_to_new_grammar_symbol_ptr_map_type =
      std::map<const GrammarSymbol*, GrammarSymbol*>;
  using CYK_vector_of_unordered_sets =
      std::vector<unordered_set_const_non_terminal_ptr>;
  using CYK_vector_of_vector_of_unordered_sets =
      std::vector<CYK_vector_of_unordered_sets>;
  using map_for_CYK_first_fill_word =
      std::map<const Terminal*, CYK_vector_of_terminal_indexed>;
  using map_for_CYK_non_terminals_relations =
      std::map<const const_non_terminal_ptr_pair,
               unordered_set_const_non_terminal_ptr>;
  using map_for_transfering_to_chomsky_remove_combined_rules_type =
      std::map<const Terminal, const NonTerminal*>;
  using map_for_transfering_to_chomsky_remove_long_rules_type =
      std::map<const string, const NonTerminal*>;
  using map_for_transfering_to_chomsky_remove_renaming_rules_type =
      std::unordered_map<const NonTerminal*,
                         unordered_set_const_non_terminal_ptr>;
  using unordered_set_for_epsilon_closing =
      std::unordered_set<const NonTerminal*>;
  using unordered_set_for_grammar_symbols =
      std::unordered_set<const GrammarSymbol*>;
  using vector_grammar_symbol_type = std::vector<const GrammarSymbol*>;
  using vector_non_terminal_type = std::vector<NonTerminal*>;
  using vector_terminal_type = std::vector<Terminal*>;
  using values_type = std::vector<vector_grammar_symbol_type>;

 public:
  static const string RULE_VALUES_SEPARATOR;
  static const string RULE_KEY_TO_VALUES_SEPARATOR;

  Rule(const string& r, const vector_terminal_type& terminals,
       const vector_non_terminal_type& non_terminals);
  Rule(const Rule& rule_to_copy,
       changing_old_to_new_grammar_symbol_ptr_map_type&
           terminals_ptr_map_old_to_new);

  bool HasToAddKeyToMarkedForIsEmptyGrammarLanguage(
      unordered_set_for_grammar_symbols& marked) const;
  bool IsValidParse(vector_grammar_symbol_type& new_vector_terminal_t,
                    const string& rule, const vector_terminal_type& terminals,
                    const vector_non_terminal_type& non_terminals);
  void FindAllEpsilonTerminals(
      unordered_set_for_epsilon_closing& epsilon_non_terminals) const;
  void FindAllRenamingRules(
      map_for_transfering_to_chomsky_remove_renaming_rules_type&
          renaming_non_terminals_map) const;
  void FirstFillSetForCYK(CYK_vector_of_vector_of_unordered_sets& T,
                          map_for_CYK_non_terminals_relations& relations,
                          const map_for_CYK_first_fill_word& word) const;
  void GenerateRulesWithRemovedEpsilonNonTerminal(
      const unordered_set_for_epsilon_closing& epsilon_non_terminals,
      const NonTerminal* start_non_terminal);
  void GenerateRulesWithRenamedNonTerminals(
      stringstream& new_rules,
      const map_for_transfering_to_chomsky_remove_renaming_rules_type&
          renaming_non_terminals_map) const;
  string GetKey() const { return key->Serialize(); }
  bool IsInChomsky(const NonTerminal* start_non_terminal) const;
  bool RemoveRenamingRules();
  virtual string Serialize() const;
  void TransferToShort(map_for_transfering_to_chomsky_remove_long_rules_type&
                           move_to_terminals_map,
                       NonTerminalManager* non_terminal_manager,
                       stringstream& new_rules_string);
  void TransferCombinedRules(
      map_for_transfering_to_chomsky_remove_combined_rules_type&
          terminals_to_non_terminals_map,
      NonTerminalManager* non_terminal_manager, stringstream& new_rules_string);

 private:
  NonTerminal* key = nullptr;
  values_type values;

  bool IsValidParseAndPush(const string& rule,
                           const vector_terminal_type& terminals,
                           const vector_non_terminal_type& non_terminals);
  void Serialize(stringstream& ss, bool skip_epsilon = false) const;
};

#endif  // OOP_AUTOMATA_SRC_RULE_H_
