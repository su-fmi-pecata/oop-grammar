#ifndef OOP_AUTOMATA_SRC_SYSTEM_H_
#define OOP_AUTOMATA_SRC_SYSTEM_H_

#include <iostream>
#include <map>
#include <string>

#include "Config.h"
#include "Grammar.h"

class System {
  using string = std::string;
  using SystemFunc = void (System::*)(string&);
  using SystemFuncMap = std::map<const string, SystemFunc>;
  using us = unsigned short;
  using GrammarsMapType = std::map<const us, Grammar*>;

 public:
  System();
  ~System();

  bool Run();

 private:
  Config config;
  SystemFuncMap functions_map;
  GrammarsMapType grammars;

  Grammar* GetGrammarById(const us id);
  Grammar* GetGrammarById(const string& id_str);
  template <class T>
  void MissingGrammarWithId(const T& id) {
    std::cout << "Missing grammar with id: " << id;
  }
  bool SeparateInputViaFirstSpace(const string& input, string& str1,
                                  string& str2);
  void ConcatenateOrUnion(string& input,
                          void (System::*operate)(Grammar* g1, Grammar* g2));
  void ConcatenateTwoGrammars(Grammar* g1, Grammar* g2);
  void RemoveQuotesFormFilename(string& filename);
  void UnionTwoGrammars(Grammar* g1, Grammar* g2);

  void AddRule(string& input);
  void Chomskify(string& input);
  void Chomsky(string& input);
  void Concat(string& input);
  void Copy(string& input);
  void CYK(string& input);
  void Discard(string& input);
  void Empty(string& input);
  void Iter(string& input);
  void List(string& input);
  void Open(string& input);
  void Print(string& input);
  void RemoveRule(string& input);
  void Save(string& input);
  void Serialize(string& input);
#ifdef _DEBUG
  void Test(string& input);
#endif  // DEBUG
  void Union(string& input);
};

#endif  // OOP_AUTOMATA_SRC_SYSTEM_H_
