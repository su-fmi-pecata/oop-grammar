/*!
  \class Validate
  \brief Class for cobject validation

  Tels if parsed object is valid or invalid with error message.

  \author Petar Toshev
 */

#ifndef OOP_AUTOMATA_SRC_VALIDATE_H_
#define OOP_AUTOMATA_SRC_VALIDATE_H_

#include <string>

class Validate {
  using string = std::string;
  using us = unsigned short;

 public:
  ~Validate() { delete[] error; }
  bool IsValid() const { return error == nullptr; }
  const char* GetError() const { return error; }

 protected:
  void SetError(const char* error_m) {
    us len = strlen(error_m);
    char* error_new = new char[len + 1]();
    memcpy(error_new, error_m, len);
    error = error_new;
  }

  void SetErrorWithCutomLeadingString(const string message,
                                      const string& wrong_data) {
    const us message_length = message.length();
    const us wrong_data_length = wrong_data.length();
    char* error_message = new char[message_length + wrong_data_length + 1]();

    memcpy(error_message, message.c_str(), message_length);
    memcpy(error_message + message_length, wrong_data.c_str(),
           wrong_data_length);
    error = error_message;
  }

 private:
  const char* error = nullptr;
};

#endif  // !OOP_AUTOMATA_SRC_VALIDATE_H_