
#include "System.h"

int main() {
  bool running = true;
  while (running) {
    System system;
    running = system.Run();
  }
}
