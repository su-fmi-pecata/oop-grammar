#include "Rule.h"

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <unordered_set>
#include <vector>

#include "Generic.h"
#include "GrammarSymbol.h"
#include "NonTerminal.h"
#include "Terminal.h"

const Rule::string Rule::RULE_VALUES_SEPARATOR = "|";
const Rule::string Rule::RULE_KEY_TO_VALUES_SEPARATOR = " -> ";

Rule::Rule(const string &r, const vector_terminal_type &terminals,
           const vector_non_terminal_type &non_terminals) {
  // parse key
  {
    // yes' it's slow, i know
    NonTerminal nt(r);
    for (NonTerminal *non_terminal : non_terminals)
      if (*non_terminal == nt) {
        key = non_terminal;
        break;
      }
    if (key == nullptr) {
      SetErrorWithCutomLeadingString(
          "Error parsing: " + r + "\nMissing key non terminal: ",
          nt.Serialize());
      return;
    }
  }
  bool reading = false;
  bool error_state = false;
  const char *c_ptr = r.c_str() + key->GetNonTerminalStringLength();
  string tmp = "";
  while (*c_ptr && !error_state) {
    const char c = *c_ptr;
    if (reading) {
      if (c == ' ') {
        // we don't like spaces
      } else if (c == '|') {
        if (IsValidParseAndPush(tmp, terminals, non_terminals)) {
          tmp = "";
        } else {
          std::cout << "WRONG RULE: " << tmp << "\n";
          error_state = true;
        }
      } else {
        tmp += c;
      }
    } else if (c == '>') {
      reading = true;
    }

    c_ptr++;
  }
  if (tmp.length())
    if (!IsValidParseAndPush(tmp, terminals, non_terminals))
      std::cout << "WRONG RULE: " << tmp << "\n";
}

Rule::Rule(const Rule &rule_to_copy,
           changing_old_to_new_grammar_symbol_ptr_map_type
               &terminals_ptr_map_old_to_new) {
  // left part
  key = (NonTerminal *)terminals_ptr_map_old_to_new.find(rule_to_copy.key)
            ->second;

  // right part
  for (const vector_grammar_symbol_type &old_vector_with_terminal_ptr :
       rule_to_copy.values) {
    vector_grammar_symbol_type new_vector_terminal;
    for (const GrammarSymbol *terminal : old_vector_with_terminal_ptr)
      new_vector_terminal.push_back(
          terminals_ptr_map_old_to_new.find(terminal)->second);
    values.push_back(new_vector_terminal);
  }
}

/*!
  @brief Find and add all epsilon non terminals

  @param (unordered_set_for_epsilon_closing &epsilon_non_terminals) all epsilon
  non terminals in grammar
  @return (void)

  Check if non terminal key can be epsilon non terminal. If yes adds it to set
  with epsilon non terminals

  @author Petar Toshev
*/
void Rule::FindAllEpsilonTerminals(
    unordered_set_for_epsilon_closing &epsilon_non_terminals) const {
  for (const vector_grammar_symbol_type &rule : values) {
    bool is_epsilon_non_terminal_key = true;
    for (const GrammarSymbol *gs : rule) {
      is_epsilon_non_terminal_key =
          is_epsilon_non_terminal_key &&
          (gs->IsEpsilon() ||
           (!gs->IsTerminal() &&
            epsilon_non_terminals.count((const NonTerminal *)gs) > 0));
      if (!is_epsilon_non_terminal_key) break;
    }
    if (is_epsilon_non_terminal_key) epsilon_non_terminals.emplace(key);
  }
}

/*!
  @brief Adds to map renaming rules

  @param (map_for_transfering_to_chomsky_remove_renaming_rules_type
        &renaming_non_terminals_map)
    Map NonTerminal* to unordered set of NonTerminals* to handle all
    renamings of non terminal.
  @return (void)

  Inserts to map pairs. If there is no such unordered set, creates new.

  @author Petar Toshev
*/
void Rule::FindAllRenamingRules(
    map_for_transfering_to_chomsky_remove_renaming_rules_type
        &renaming_non_terminals_map) const {
  for (const vector_grammar_symbol_type &rule : values)
    if (rule.size() == 1 && !(rule[0]->IsTerminal()) && key != rule[0]) {
      map_for_transfering_to_chomsky_remove_renaming_rules_type::iterator it =
          renaming_non_terminals_map.find(key);
      if (it == renaming_non_terminals_map.end()) {
        unordered_set_const_non_terminal_ptr un_s;
        renaming_non_terminals_map.emplace(key, un_s);
        it = renaming_non_terminals_map.find(key);
      }

      it->second.emplace((const NonTerminal *)rule[0]);
    }
}

/*!
  @brief Fill First set for CYK algo

  @param (CYK_vector_of_unordered_sets &first_set) first set to be filled
  @return (void)

  @warning This alghorithm works only on grammars in Chomsky format.

  Iterates throught all rule parts by key. Adds into relation for nonterminal
  pairs [A -> BC] creates pair<B,C> and adds A (A == this->key) to set. Adds
  this->key to T[i][1] set (if A->a, and a is from word), where i is index of
  a in word (from word map).

  @author Petar Toshev
*/
void Rule::FirstFillSetForCYK(
    CYK_vector_of_vector_of_unordered_sets &T,
    map_for_CYK_non_terminals_relations &relations,
    const map_for_CYK_first_fill_word &word_map) const {
  for (const vector_grammar_symbol_type &value : values) {
    if (value.size() == 1) {
      // fill map with terminals and indexes
      map_for_CYK_first_fill_word::const_iterator it =
          word_map.find((const Terminal *)value[0]);
      if (it == word_map.end()) continue;
      const CYK_vector_of_terminal_indexed &indexes = it->second;
      for (const us &index : indexes) T[index][0].emplace(key);
    } else if (value.size() == 2) {
      // fill map with non terminals relations
      const const_non_terminal_ptr_pair map_key = {
          (const NonTerminal *)value[0], (const NonTerminal *)value[1]};
      map_for_CYK_non_terminals_relations::iterator it =
          relations.find(map_key);
      if (it == relations.end()) {
        unordered_set_const_non_terminal_ptr set;
        relations.emplace(map_key, set);
        it = relations.find(map_key);
      }
      it->second.emplace(this->key);
    }
  }
}

/*!
  @brief Generate all rules with removed epsilon non terminals

  @param (unordered_set_for_epsilon_closing &epsilon_non_terminals) all epsilon
  non terminals in grammar
  @return (void)

  Generates all possible varrianst for rules with removed epsilon non terminals.
  Actually our rule parsing ctr does NOT allow to have Epsilons in rule with
  terminal or non terminals. It only allows only epsilon or only (terminals, non
  terminal)+. Because of that reason we know that if GrammarSymbol is Epsilon it
  is only one in the rule. So we have to check if it is in rule with key start
  non_terminal. Else we can remove thar rule part.
  For every epsilon non terminal creates copy of rule without this non termial.
  Uses serialized set for removing all duplicate rules.

  @author Petar Toshev
*/
void Rule::GenerateRulesWithRemovedEpsilonNonTerminal(
    const unordered_set_for_epsilon_closing &epsilon_non_terminals,
    const NonTerminal *start_non_terminal) {
  // TODO: Petar Toshev (11.06.2019) [Move first vector to unordered_map]
  // Move vector<vector<GrammarSymbol*>> to
  // unordered_map<vector<GrammarSymbol*>> to remove repeated rules

  // temp duplicates removing
  std::unordered_set<string> set_for_removing_duplicates;
  stringstream ss_rule_string;

  for (us i = 0; i < values.size(); i++) {
    // fill map with fist of values
    {
      // generate serialized string
      ss_rule_string.str("");
      ss_rule_string.clear();
      const vector_grammar_symbol_type &rule = values[i];
      for (const GrammarSymbol *gs : rule) ss_rule_string << gs->Serialize();
      const string &serialized = ss_rule_string.str();

      // remove value and set index to previous
      if (set_for_removing_duplicates.count(serialized)) {
        values.erase(values.begin() + i);
        i--;
        continue;
      }

      // add to set
      set_for_removing_duplicates.emplace(serialized);
    }

    const us rule_size = values[i].size();
    for (us r = 0; r < rule_size; r++) {
      // something is moving in memori, so we have to set it here
      const vector_grammar_symbol_type &rule = values[i];
      const GrammarSymbol *gs = rule[r];
      if (gs->IsEpsilon() && key != start_non_terminal) {
        // if has epsilon and is non start, well, remove rule
        values.erase(values.begin() + i);
        i--;
        break;
      } else if (!gs->IsTerminal() &&
                 epsilon_non_terminals.count((const NonTerminal *)gs) > 0 &&
                 rule.size() > 1) {
        // is epsilon non terminal so have to copy whole vector and remove this
        // terminal from new one and has atleast 2 elements

        vector_grammar_symbol_type new_rule_vector(rule);
        new_rule_vector.erase(new_rule_vector.begin() + r);
        values.push_back(new_rule_vector);
      }
    }
  }
}

/*!
  @brief Adds all renamed values with this rule

  @param (stringstream &new_rules) where rules to be saved
  @param (const map_for_transfering_to_chomsky_remove_renaming_rules_type
        &renaming_non_terminals_map) map with renaming pairs
*/
void Rule::GenerateRulesWithRenamedNonTerminals(
    stringstream &new_rules,
    const map_for_transfering_to_chomsky_remove_renaming_rules_type
        &renaming_non_terminals_map) const {
  map_for_transfering_to_chomsky_remove_renaming_rules_type::const_iterator
      it_map = renaming_non_terminals_map.find(key);
  // if we don't have what to copy
  if (it_map == renaming_non_terminals_map.end()) return;

  // get reference (a bit faster)
  const unordered_set_const_non_terminal_ptr &set = it_map->second;

  // serialize rule
  stringstream serialized_rule_stream;
  serialized_rule_stream << RULE_KEY_TO_VALUES_SEPARATOR;
  Serialize(serialized_rule_stream, true);
  serialized_rule_stream << std::endl;
  // calculate rule string length
  const us default_empty_rule_length =
      RULE_KEY_TO_VALUES_SEPARATOR.length() + 1 /*endl*/;
  // get reference of rule string
  const string &serialized_rule = serialized_rule_stream.str();
  // check if is not empty
  if (serialized_rule.size() <= default_empty_rule_length) return;

  // iterate all elements
  unordered_set_const_non_terminal_ptr::const_iterator it_set = set.begin();
  while (it_set != set.end())
    new_rules << (*it_set++)->Serialize() << serialized_rule;
}

/*!
  @brief Check if rule is in Chomsky

  @param (NonTerminal*) start_non_terminal start_non_terminal in grammar
  @return (bool) Is Rule in Chomsky

  Check if rule is in Chomsky standart.

  @author Petar Toshev
*/
bool Rule::IsInChomsky(const NonTerminal *start_non_terminal) const {
  bool res = true;
  for (const vector_grammar_symbol_type &terminals : values) {
    bool only_one_terminal_or_epsilon_on_start_non_terminal =
        terminals.size() == 1 && terminals[0]->IsTerminal() &&
        (!terminals[0]->IsEpsilon() || *key == *start_non_terminal);
    bool two_non_terminals = terminals.size() == 2 &&
                             !terminals[0]->IsTerminal() &&
                             !terminals[1]->IsTerminal();
    res = res && (only_one_terminal_or_epsilon_on_start_non_terminal ||
                  two_non_terminals);
    if (!res) break;
  }
  return res;
}

/*!
  @brief Removes all renaming (single) non terminals
  @return (bool) Is rule empty
  Removes all single non terminals from right part of rule. Returns is rule
  empty.

  @author Petar Toshev
*/
bool Rule::RemoveRenamingRules() {
  for (us i = 0; i < values.size(); i++) {
    if (values[i].size() == 1 && !values[i][0]->IsTerminal()) {
      values.erase(values.begin() + i);
      i--;
    }
  }
  return values.empty();
}

/*!
  @brief Serializes rule in format ready to be printed

  @param void
  @return (string) Serialized rule in string

  Serializes rule in format, ready to be printed and parsed

  @author Petar Toshev
*/
Rule::string Rule::Serialize() const {
  stringstream ss;
  ss << GetKey() << RULE_KEY_TO_VALUES_SEPARATOR;
  Serialize(ss);
  return ss.str();
}

/*!
  @brief Move long rules to short

  @param (map_for_transfering_to_chomsky_remove_long_rules_type
        &move_to_terminals_map)
    Adding string with transfered grammar symbols to new non terminal
  @param (NonTerminalManager *non_terminal_manager)
    NonTermianl Manager interface of grammar
  @param (stringstream& new_rules_string) new rules to be added in grammar
  @return (void)

  Iterates all rule parts;
  Creates string with part of the rule part and removes them (from [1] to the
  end of rule part). Then checks if already exist such part of rule part. If yes
  sets last (second) element to be already existing non_terminal. If not creates
  new non_terminals and add string with part to adding new rules string.

  @author Petar Toshev
*/
void Rule::TransferToShort(map_for_transfering_to_chomsky_remove_long_rules_type
                               &move_to_terminals_map,
                           NonTerminalManager *non_terminal_manager,
                           stringstream &new_rules_string) {
  stringstream rule_stringified;
  for (vector_grammar_symbol_type &rule : values) {
    if (rule.size() <= 2) continue;
    // reset string streams
    rule_stringified.str("");
    rule_stringified.clear();
    // add stringified grammar symbols to string stream and clear terminals from
    // vector
    {
      const us rule_len = rule.size();
      for (us i = 1; i < rule_len; i++)
        rule_stringified << rule[i]->Serialize();
      rule.erase(rule.begin() + 1, rule.end());
    }

    const NonTerminal *new_non_terminal;
    const string &rule_stringified_string = rule_stringified.str();
    // check if there is terminal for this sequence
    // if there is none, create new
    map_for_transfering_to_chomsky_remove_long_rules_type::iterator it =
        move_to_terminals_map.find(rule_stringified_string);
    if (it != move_to_terminals_map.end()) {
      // already added separated rule
      new_non_terminal = it->second;
    } else {
      new_non_terminal =
          non_terminal_manager
              ->CreateAndInsertNewNonTerminalWithFirstFreeNumberViaLetter(
                  key->GetLetter());
      move_to_terminals_map.emplace(rule_stringified_string, new_non_terminal);

      // have to add separated rule
      // add to new rule string
      new_rules_string << new_non_terminal->Serialize();
      new_rules_string << RULE_KEY_TO_VALUES_SEPARATOR;
      new_rules_string << rule_stringified_string;
      new_rules_string << std::endl;
    }

    rule.push_back(new_non_terminal);
  }
}

/*!
  @brief Chnage terminals to non terminals with rule for terminals

  @param (map_for_transfering_to_chomsky_remove_combined_rules_type
        &terminals_to_non_terminals_map)
    Map with terminal to non terminal equivalent
  @param (NonTerminalManager *non_terminal_manager)
    NonTermianl Manager interface of grammar
  @param (stringstream& new_rules_string) new rules to be added in grammar
  @return (void)

  Iterates all rule parts. If there is terminal and size of grammar symbolsin
  rule part is more than one replaces it with non terminal equivalent. If there
  has not been created non terminal equivalen creates one via @ref
  NonTerminalManager::CreateAndInsertNewNonTerminalWithFirstFreeNumberViaLetter

  @author Petar Toshev
*/
void Rule::TransferCombinedRules(
    map_for_transfering_to_chomsky_remove_combined_rules_type
        &terminals_to_non_terminals_map,
    NonTerminalManager *non_terminal_manager, stringstream &new_rules_string) {
  stringstream rule_stringified;
  for (vector_grammar_symbol_type &rule : values) {
    bool has_more_than_one_symbol = rule.size() > 1;
    const us rule_size = rule.size();
    for (us i = 0; i < rule_size; i++) {
      const GrammarSymbol *gs = rule[i];
      if (has_more_than_one_symbol && gs->IsTerminal()) {
        const NonTerminal *replacement = nullptr;
        map_for_transfering_to_chomsky_remove_combined_rules_type::iterator it =
            terminals_to_non_terminals_map.find(*(const Terminal *)gs);
        if (it != terminals_to_non_terminals_map.end()) {
          replacement = it->second;
        } else {
          // create new non tereminal and add it to map
          replacement =
              non_terminal_manager
                  ->CreateAndInsertNewNonTerminalWithFirstFreeNumberViaLetter(
                      key->GetLetter());
          terminals_to_non_terminals_map.emplace(*(const Terminal *)gs,
                                                 replacement);

          // create new rule string
          new_rules_string << replacement->Serialize()
                           << RULE_KEY_TO_VALUES_SEPARATOR << gs->Serialize()
                           << std::endl;
        }

        rule[i] = replacement;
      }
    }
  }
}

/*!
  @brief Checks if key can be added to marked

  @param (unordered_set_for_grammar_symbols &marked) already marked symbols
  @return (bool) Has added element to marked

  Adds key to marked if all symbols of rule any rule part are in marked.

  @author Petar Toshev
*/
bool Rule::HasToAddKeyToMarkedForIsEmptyGrammarLanguage(
    unordered_set_for_grammar_symbols &marked) const {
  if (marked.count(key)) return false;
  for (const vector_grammar_symbol_type &rule : values) {
    bool are_all_marked = rule.size();
    for (const GrammarSymbol *symbol : rule)
      are_all_marked = are_all_marked && marked.count(symbol);
    if (are_all_marked) {
      marked.emplace(key);
      return true;
    }
  }
  return false;
}

/*!
  @brief Check if rule in string is valid and parses it

  @param (vector_grammar_symbol_type &new_vector_terminal_t)
  vector with Terminal ptr in grammar (where rule will be parsed)
  @param (const string &rule) rule in string which will be parsed
  @param (const vector_terminal_type &terminals)
  vector with Terminals ptr in grammar
  @param (const vector_non_terminal_type &non_terminals)
  vector with NonTerminals ptr in grammar
  @return (bool) Is rule valid (size is more than 0 and zero errors)

  @warning Can throw error via Validate interface.

  Checking if rule is valid and if it is -> parses it in format
  for using by the system. Creates a TErminal or NonTerminal of
  every char in rule string and check if it's in grammars.
  Skips epsilons if there are more terminals or NonTerminals in rule

  @author Petar Toshev
*/
bool Rule::IsValidParse(vector_grammar_symbol_type &new_vector_terminal_t,
                        const string &rule,
                        const vector_terminal_type &terminals,
                        const vector_non_terminal_type &non_terminals) {
  const char *arr = rule.c_str();
  const char *end_ptr = rule.c_str() + rule.size();
  us len_to_skip = 7;  // random number > 0, doesn't matter
  bool is_previous_epsilon = false;
  while (*arr && len_to_skip) {
    GrammarSymbol *to_be_added = nullptr;

    // check if terminal
    if (!terminals.empty()) {
      // yes it's slow I know
      Terminal t(arr[0]);
      for (Terminal *terminal : terminals)
        if (*terminal == t) {
          to_be_added = terminal;
          len_to_skip = 1;
          break;
        }
    }

    // check if non_terminal or epsilon
    if (!non_terminals.empty() && to_be_added == nullptr) {
      // yes it's slow I know
      NonTerminal nt(arr, end_ptr - arr);
      for (NonTerminal *non_terminal : non_terminals)
        if (*non_terminal == nt) {
          to_be_added = non_terminal;
          len_to_skip = nt.GetNonTerminalStringLength();
          break;
        }
    }

    // check if we have what to add
    if (to_be_added == nullptr) {
      SetErrorWithCutomLeadingString("Error parsing: " + rule + "\nOn part: ",
                                     arr);
      return false;
    }

    if (is_previous_epsilon) new_vector_terminal_t.pop_back();
    new_vector_terminal_t.push_back(&(*to_be_added));
    arr += len_to_skip;
    is_previous_epsilon = to_be_added->IsEpsilon();
  }
  if (new_vector_terminal_t.size() > 1 && is_previous_epsilon)
    new_vector_terminal_t.pop_back();

  return new_vector_terminal_t.size();
}

bool Rule::IsValidParseAndPush(const string &rule,
                               const vector_terminal_type &terminals,
                               const vector_non_terminal_type &non_terminals) {
  vector_grammar_symbol_type new_vector_terminal_t;
  bool res =
      IsValidParse(new_vector_terminal_t, rule, terminals, non_terminals);
  if (res) values.push_back(new_vector_terminal_t);
  return res;
}

void Rule::Serialize(stringstream &ss, bool skip_epsilon) const {
  bool already_added_rule = false;
  for (const vector_grammar_symbol_type &tmp : values) {
    if (already_added_rule) ss << RULE_VALUES_SEPARATOR;
    for (const GrammarSymbol *symbol : tmp)
      if (!symbol->IsEpsilon() || (symbol->IsEpsilon() && !skip_epsilon)) {
        ss << symbol->Serialize();
        already_added_rule = true;
      }
  }
}
