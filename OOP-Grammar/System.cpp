#include "System.h"

#include <fstream>
#include <iostream>

System::System() {
  functions_map.emplace("AddRule", &System::AddRule);
  functions_map.emplace("Chomskify", &System::Chomskify);
  functions_map.emplace("Chomsky", &System::Chomsky);
  functions_map.emplace("Concat", &System::Concat);
  functions_map.emplace("Copy", &System::Copy);
  functions_map.emplace("CYK", &System::CYK);
  functions_map.emplace("Discard", &System::Discard);
  functions_map.emplace("Empty", &System::Empty);
  functions_map.emplace("Iter", &System::Iter);
  functions_map.emplace("List", &System::List);
  functions_map.emplace("Open", &System::Open);
  functions_map.emplace("Print", &System::Print);
  functions_map.emplace("RemoveRule", &System::RemoveRule);
  functions_map.emplace("Save", &System::Save);
  functions_map.emplace("Serialize", &System::Serialize);
#ifdef _DEBUG
  functions_map.emplace("Test", &System::Test);
#endif  // DEBUG
  functions_map.emplace("Union", &System::Union);
}

System::~System() {}

bool System::Run() {
  {  // read welcome message
    const string welcome_message = "../Welcome.txt";
    std::ifstream infile(welcome_message.c_str());
    string line;
    while (std::getline(infile, line)) std::cout << line << std::endl;
  }

  string input, command, params;
  while (true) {
    std::cout << "\n\nwaiting...\n";
    std::getline(std::cin, input);

    {  // separate command and params
      if (!SeparateInputViaFirstSpace(input, command, params)) {
        command = input;
        params = "";
      }

      if (config.IsDeveloperMode())
        std::cout << "command: --" << command << "--\nparams : --" << params
                  << "--\n";
    }

    SystemFuncMap::iterator function = functions_map.find(command);
    if (function != functions_map.end()) {
      (this->*(function->second))(params);
    } else if (input == "Restart") {
      std::cout << "Restarting...\n\n\n\n";
      return true;
    } else if (input == "Exit") {
      std::cout << "Stopping...\n\n\n\n";
      return false;
    }

    // TODO: Petar Toshev [Automata for commands]
    // Create automata for comands
    // Create function pointers for executing from commands
  }

  // well we will never come here, but just to be sure
  return false;
}

/*!
  @brief Get pointer to grammar via grammar's id.

  @param (us) id Grammar's id
  @return Grammar* or nullptr (if there is no grammar with this id
  
  Iterates through map of grammars to find grammar with this id;
  Return nullptr if there is no grammar with this id.
*/
Grammar* System::GetGrammarById(const us id) {
  GrammarsMapType::iterator grammar_it = grammars.find(id);
  if (grammar_it != grammars.end()) return grammar_it->second;

  return nullptr;
}

/*!
  @brief Get pointer to grammar via grammar's id.

  @param (string) id_str Grammar's id
  @return Grammar* or nullptr (if there is no grammar with this id)

  Uses @ref Grammar* System::GetGrammarById(const us id)
*/
Grammar* System::GetGrammarById(const string& id_str) {
  return GetGrammarById(std::stoi(id_str));
}

/*!
  @brief Separates string into two pieces (space is separator)

  @param (string) input value to be separated
  @param (string&) str1 part 1
  @param (string&) str2 part 2
  @return (bool) Is separating successfully

  Separates string input into two pieces via first found space
  (left to right).
*/
bool System::SeparateInputViaFirstSpace(const string& input, string& str1,
                                        string& str2) {
  string::size_type index_of_first_space = input.find(" ", 0);
  if (index_of_first_space == string::npos) return false;
  str1 = input.substr(0, index_of_first_space);
  str2 = input.substr(index_of_first_space + 1,
                      input.length() - index_of_first_space);
  return true;
}

/*!
  @brief Function for concatenate or union two grammars.

  @param (string&) input Recieves two grammars id's in format id1 id2
  @param void (System::*)(Grammar* g1, Grammar* g2)
  Function for concatenationg or union twoo grammars
  @return void

  Creates copy of grammar with id1 in dynamic memory.
  Then creates copy of second grammar (id2). Renames repeated
  NonTerminals and extends terminals. Calls function operate to
  do the job.
*/
void System::ConcatenateOrUnion(string& input,
                                void (System::*operate)(Grammar* g1,
                                                        Grammar* g2)) {
  string id1_string, id2_string;
  if (!SeparateInputViaFirstSpace(input, id1_string, id2_string)) {
    std::cout << "Cannot parse id's\n";
    return;
  }

  Grammar* grammar1 = GetGrammarById(id1_string);
  Grammar* grammar2 = GetGrammarById(id2_string);

  if (grammar1 == nullptr) {
    std::cout << "Wrong id of first grammar: " << id1_string << std::endl;
    return;
  }

  if (grammar2 == nullptr) {
    std::cout << "Wrong id of second grammar: " << id2_string << std::endl;
    return;
  }

  grammar1 = new Grammar(*grammar1);
  (this->*operate)(grammar1, grammar2);
  if (grammar1->IsValid()) {
    grammars.insert({grammar1->GetId(), grammar1});
    std::cout << "New grammar id: " << grammar1->GetId();
  } else {
    std::cout << "Cannot do operation grammars with ids: " << input;
    std::cout << std::endl << grammar1->GetError();
  }
}

void System::ConcatenateTwoGrammars(Grammar* g1, Grammar* g2) {
  g1->ConcatenateWith(*g2);
}

/*!
  @brief Removes " from filenames

  @param (string& filename) filename where to remove quotes
  @return void

  Removes leading and closing ".

  @author Petar Toshev
*/
void System::RemoveQuotesFormFilename(string& filename) {
  const char quote = '"';
  if (filename[0] == quote) filename = filename.substr(1);
  us filename_len = filename.length();
  if (filename[filename_len - 1] == quote)
    filename = filename.substr(0, filename_len - 1);
}

void System::UnionTwoGrammars(Grammar* g1, Grammar* g2) {
    g1->UnionWith(*g2); }
