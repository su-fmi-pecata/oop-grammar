#ifndef OOP_AUTOMATA_SRC_CONFIG_H_
#define OOP_AUTOMATA_SRC_CONFIG_H_

class Config {
  using default_int = unsigned short;

 public:
  default_int GetNextGrammarId() { return grammars++; }
  bool IsDeveloperMode() { return is_developer_mode; }

 private:
  const bool is_developer_mode =
#ifdef _DEBUG
      true;
#else
      false;
#endif
  default_int grammars = 0;
};

#endif  // OOP_AUTOMATA_SRC_CONFIG_H_
