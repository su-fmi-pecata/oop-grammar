#include "Grammar.h"

#include <math.h>
#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include <unordered_map>
#include <unordered_set>

// Grammar::Grammar(Config& config, rule_vector_t& rules, NonTerminal& start,
//                 const char* non_terminals, const char* terminals)
//    : Generic(/*config, */ config.GetNextGrammarId()) {}
Grammar::Grammar(Config& config, ifstream& infile)
    : Generic(config, config.GetNextGrammarId()) {
  string line;

  // NonTerminals
  if (std::getline(infile, line)) {
    if (line.length()) {
      const us skip_space = 1;
      const char* arr = line.c_str();
      while (*arr) {
        us len = 0;
        while (arr[len] && arr[len] != ' ') len++;
        NonTerminal* nt = new NonTerminal(arr, len);
        non_terminals.push_back(nt);
        AddNewNonTerminalToMapWithFreeNumbers(*nt);
        arr += len + (arr[len] ? skip_space : 0);
      }
    } else {
      us len = (sizeof(DEFAULT_NON_TERMINALS) / sizeof(*DEFAULT_NON_TERMINALS));
      for (us i = 0; i < len; i++)
        non_terminals.push_back(new NonTerminal(DEFAULT_NON_TERMINALS + i, 0));
    }
    // std::unordered_set<NonTerminal> non_terminals_set;
  }

  //// NonTerminals NEW
  // if (!FillVectorWithTerminalsOrNonTerminals<NonTerminal>(
  //        infile, non_terminals, DEFAULT_NON_TERMINALS)) {
  //  SetError("Error parsing non_terminals! Check for duplicated values");
  //  return;
  //}

  // Terminals
  if (std::getline(infile, line)) {
    if (line.length()) {
      const us skip_space = 1;
      const char* arr = line.c_str();
      while (*arr) {
        us len = 0;
        while (arr[len] && arr[len] != ' ') len++;
        terminals.push_back(new Terminal(arr[0]));
        arr += len + (arr[len] ? skip_space : 0);
      }
    } else {
      us len = (sizeof(DEFAULT_TERMINALS) / sizeof(*DEFAULT_TERMINALS));
      for (us i = 0; i < len; i++)
        terminals.push_back(new Terminal(DEFAULT_TERMINALS[i]));
    }
  }
  //// Terminals NEW
  // if (!FillVectorWithTerminalsOrNonTerminals<Terminal>(infile, terminals,
  //                                                     DEFAULT_NON_TERMINALS))
  //                                                     {
  //  SetError("Error parsing terminals! Check for duplicated values");
  //  return;
  //}

  // StartNonTerminal
  if (std::getline(infile, line) && line.length())
    start_non_terminal = new NonTerminal(line);
  else
    start_non_terminal = new NonTerminal(&DEFAULT_START_NON_TERMINAL);

  // Epsilon
  EpsilonTerminal* epsilon_editable;
  if (std::getline(infile, line) && line.length()) {
    const char* row = line.c_str();
    while (*row == ' ') row++;
    epsilon_editable = new EpsilonTerminal(*row);
  } else
    epsilon_editable = new EpsilonTerminal(EPSILON);

  // Check if intersection of terminals and non_terminals are empty set
  // And is valid start terminal
  // And add epsilon to terminals
  {
    std::unordered_map<NonTerminal, bool /*is NonTerminal*/> non_terminals_map;
    for (const Terminal* terminal : terminals) {
      NonTerminal nt(&terminal->GetLetter(), 0);
      std::unordered_map<NonTerminal, bool /*is NonTerminal*/>::iterator it =
          non_terminals_map.find(nt);
      if (it == non_terminals_map.end()) {
        non_terminals_map.insert({nt, false});
      } else {
        SetErrorWithCutomLeadingString("Repeated terminal character: ",
                                       nt.Serialize());
        return;
      }
    }
    for (const NonTerminal* non_terminal : non_terminals) {
      NonTerminal nt(*non_terminal);
      std::unordered_map<NonTerminal, bool /*is NonTerminal*/>::iterator it =
          non_terminals_map.find(nt);
      if (it == non_terminals_map.end()) {
        non_terminals_map.insert({NonTerminal(*non_terminal), true});
      } else if (it->second) {
        SetErrorWithCutomLeadingString("Repeated non_terminal character: ",
                                       nt.Serialize());
        return;
      } else {
        SetErrorWithCutomLeadingString(
            "Ooopss! The intersection of terminals and non_terminals is not "
            "empty :/",
            nt.Serialize());
        return;
      }
    }

    // check for start_non_terminal
    // make start_non_terminal ptr to it's non_terminal
    {
      // check
      std::unordered_map<NonTerminal, bool /*is NonTerminal*/>::const_iterator
          check_for_start = non_terminals_map.find(*start_non_terminal);
      if (check_for_start == non_terminals_map.end()) {
        SetError("Missing start_non_terminal in non_terminals declaration");
        return;
      }
      if (!check_for_start->second) {
        SetError("Start_non_terminal is in terminal declaration");
        return;
      }

      // make
      for (NonTerminal* nt : non_terminals)
        if (*nt == *start_non_terminal) {
          delete start_non_terminal;
          start_non_terminal = nt;
          break;
        }
    }

    // add better epsilon to terminals :D
    // check if epsilon is in non_terminals
    {
      bool has_to_be_removed_first = true;
      NonTerminal nt_epsilon(&epsilon_editable->GetLetter(), 1);
      std::unordered_map<NonTerminal, bool /*is NonTerminal*/>::const_iterator
          check_epsilon = non_terminals_map.find(nt_epsilon);
      if (check_epsilon == non_terminals_map.end()) {
        has_to_be_removed_first = false;
      } else if (check_epsilon->second) {
        SetError("Epsilon is in non_terminal declaration");
        return;
      }

      // remove if is here
      if (has_to_be_removed_first)
        for (NonTerminal* nt : non_terminals)
          if (*nt == nt_epsilon) {
            delete nt;
            break;
          }
      terminals.push_back((Terminal*)epsilon_editable);
      epsilon = epsilon_editable;
    }
  }

  // Rules
  while (std::getline(infile, line)) {
    if (line.length()) {
      if (!AddRule(line)) {
        SetError("\nGrammar cannot be parsed with wrong rule... :/");
        return;
      }
    }
  }
}

/*!
  @brief Grammar copy ctr

  @param (Grammar& grammar_to_copy) Grammar to be copied

  Copies grammars. Creates copy into heap for every terminal
  and non_terminal. Creates map for every old place in heap
  to new place in heap and change all rules to use new address

  @author Petar Toshev
*/
Grammar::Grammar(Grammar& grammar_to_copy)
    : Generic(*grammar_to_copy.GetConfig(),
              grammar_to_copy.GetConfig()->GetNextGrammarId()) {
  // copy map with non_terminals used letters and numbers
  this->non_terminals_first_free_map =
      grammar_to_copy.non_terminals_first_free_map;

  changing_old_to_new_grammar_symbol_ptr_map_type
      grammar_symbols_ptr_map_old_to_new;
  for (Terminal* old_terminal : grammar_to_copy.terminals) {
    Terminal* new_terminal;
    // copy EpsilonTerminals too
    if (old_terminal->IsEpsilon())
      new_terminal = new EpsilonTerminal(*(EpsilonTerminal*)old_terminal);
    else
      new_terminal = new Terminal(*old_terminal);
    // add new terminal to list
    terminals.push_back(new_terminal);
    // and to renaming map
    grammar_symbols_ptr_map_old_to_new.insert({old_terminal, new_terminal});
  }
  // add non_terminals to new address in heap (copy) and add them to rename map
  for (NonTerminal* old_non_terminal : grammar_to_copy.non_terminals) {
    NonTerminal* new_non_terminal = new NonTerminal(*old_non_terminal);
    non_terminals.push_back(new_non_terminal);
    grammar_symbols_ptr_map_old_to_new.insert(
        {old_non_terminal, new_non_terminal});
  }

  // set start_non_terminal to new address
  start_non_terminal = (const NonTerminal*)grammar_symbols_ptr_map_old_to_new
                           .find(grammar_to_copy.start_non_terminal)
                           ->second;

  // set epsilon to new address
  epsilon = (const EpsilonTerminal*)grammar_symbols_ptr_map_old_to_new
                .find(grammar_to_copy.epsilon)
                ->second;

  // change rules address
  for (const Rule& rule_to_copy : grammar_to_copy.rules) {
    rules.push_back(Rule(rule_to_copy, grammar_symbols_ptr_map_old_to_new));
  }
}

Grammar::~Grammar() {
  for (Terminal* t : terminals) delete t;
  for (NonTerminal* nt : non_terminals) delete nt;
}

/*!
  @brief Function for adding rule to grammar.

  @param (const string&) rule rule in string format
  @return (bool) Is successfull parsing rule and adding it
  to grammar

  @author Petar Toshev
*/
bool Grammar::AddRule(const string& rule) {
  Rule new_rule(rule, terminals, non_terminals);
  bool res = new_rule.IsValid();
  if (res)
    rules.push_back(new_rule);
  else
    std::cout << new_rule.GetError() << std::endl;
  return res;
}

/*!
  @brief Function for combining two grammars.

  @param (Grammar&) grammar_to_combine_with
  @return (NonTerminal*) Pointer to strat_non_terminal for
  second grammar in this grammar's non_terminals

  Combines grammar2 into this grammar. Renames NonTerminals in
  grammar2 (if needed). Changes epsilon of grammar 2 to epsilon
  in grammar 1.

  @author Petar Toshev
*/
NonTerminal* Grammar::CombineWith(Grammar& grammar_to_combine_with) {
  Grammar grammar2(grammar_to_combine_with);
  Grammar& grammar1 = *this;

  // TODO: Petar Toshev (03.06.2019) [intersection combining grammars]
  // Check intersection of grammar 2 terminals and grammar 1 non_terminals
  // Check intersection of grammar 1 terminals and grammar 2 non_terminals

  std::unordered_set<NonTerminal> non_terminals_intersection;

  // fills non_terminals_number with non_terminals from grammar 1
  // fills non_terminals_intersection with non_terminals
  // from grammar 1
  for (NonTerminal* nt : grammar1.non_terminals) {
    const char& letter = nt->GetLetter();
    const us& number = nt->GetNumber();
    non_terminals_first_free_map_type::iterator it =
        non_terminals_first_free_map.find(letter);
    if (it == non_terminals_first_free_map.end())
      non_terminals_first_free_map.insert({letter, number + 1});
    else
      it->second = std::max((int)it->second, number + 1);

    non_terminals_intersection.insert(*nt);
  }

  changing_old_to_new_grammar_symbol_ptr_map_type
      grammar2_renaming_terminals_ptr_map;

  // fills non_terminals_number with non_terminals from grammar 2
  // fills non_terminals_intersection with non_terminals
  // from grammar 2
  // renames non_terminals if needed (from grammar 2)
  for (NonTerminal* nt : grammar2.non_terminals) {
    const char& letter = nt->GetLetter();
    const us& number = nt->GetNumber();
    non_terminals_first_free_map_type::iterator it =
        non_terminals_first_free_map.find(letter);
    if (it == non_terminals_first_free_map.end())
      non_terminals_first_free_map.insert({letter, number + 1});
    else
      it->second = std::max((int)it->second, number + 1);

    us non_terminals_intersection_size = non_terminals_intersection.size();
    non_terminals_intersection.insert(*nt);
    if (non_terminals_intersection.size() == non_terminals_intersection_size) {
      // well we have to change this nonterminal
      nt->SetNumber(it->second++);
    }

    // add grammar2 non_terminal to grammar1 non_terminals
    NonTerminal* new_non_terminal = new NonTerminal(*nt);
    grammar1.non_terminals.push_back(new_non_terminal);

    // add renaming to map
    grammar2_renaming_terminals_ptr_map.insert({nt, new_non_terminal});
  }

  // add terminals from grammar 1 to map
  std::map<Terminal, Terminal*> terminals_map_with_pointers;
  for (Terminal* t : grammar1.terminals)
    terminals_map_with_pointers.insert({*t, t});

  // add terminals from grammar 2 to map with terminal
  // add pointers in grammar2_renaming_terminals_ptr_map
  // skips adding or grammar 2 epsilon
  for (Terminal* t : grammar2.terminals) {
    Terminal* new_terminal_to_add_to_map;
    if (t->IsEpsilon()) {
      // set epsilon from grammar 2 to epsilon in grammar 1
      new_terminal_to_add_to_map = (Terminal*)epsilon;
    } else {
      std::map<Terminal, Terminal*>::iterator it =
          terminals_map_with_pointers.find(*t);
      if (it == terminals_map_with_pointers.end()) {
        // doesn't exist and have to add
        new_terminal_to_add_to_map = new Terminal(*t);
        terminals.push_back(new_terminal_to_add_to_map);
        terminals_map_with_pointers.insert({*t, new_terminal_to_add_to_map});
      } else {
        new_terminal_to_add_to_map = it->second;
      }
    }

    grammar2_renaming_terminals_ptr_map.insert({t, new_terminal_to_add_to_map});
  }

  // add rules from grammar 2 to grammar 1
  for (const Rule& rule_to_copy : grammar2.rules) {
    rules.push_back(Rule(rule_to_copy, grammar2_renaming_terminals_ptr_map));
  }

  // return converted start terminal
  return (NonTerminal*)grammar2_renaming_terminals_ptr_map
      .find(grammar2.start_non_terminal)
      ->second;
}

/*!
  @brief Concatenate or union this grammar with other grammar

  @param (Grammar&) grammar2 grammar to be concatenated/unioned with
  @return void

  @warning Can throw error via Validate interface
*/
void Grammar::ConcatenateOrUnion(Grammar& grammar2, const char separator) {
  const NonTerminal* grammar1_start_non_terminal_ptr = start_non_terminal;
  const NonTerminal* grammar2_start_non_terminal_ptr = CombineWith(grammar2);
  start_non_terminal =
      CreateAndInsertNewNonTerminalWithFirstFreeNumberViaLetter(
          start_non_terminal->GetLetter());
  string rule = "";
  rule += start_non_terminal->Serialize();
  rule += " -> ";
  rule += grammar1_start_non_terminal_ptr->Serialize();
  if (separator) rule += separator;
  rule += grammar2_start_non_terminal_ptr->Serialize();
  if (!AddRule(rule)) {
    SetError("\nGrammar cannot be parsed with wrong rule... :/");
    return;
  }
}

/*!
  @brief Concatenate this grammar with other grammar

  @param (Grammar&) grammar2 grammar to be concatenated with
  @return void

  @warning Can throw error via Validate interface

  Uses @ref Grammar::ConcatenateOrUnion(Grammar& grammar2, const char separator)
*/
void Grammar::ConcatenateWith(Grammar& grammar2) {
  ConcatenateOrUnion(grammar2, 0);
}

/*!
  @brief CYK algorhythm

  @param (const string& word) word to check is in grammar language
  @return (bool) Is word in language

  Checks if word is in language of grammar.
  Separate word to const GrammarSymbols*. If there is NonTerminal in
  symbols breaks.
  Generates map<const Terminal*, vector<us>>. Creates map for terminals
  and is's position into word (index). (One terminal can be on more than one
  place). (@ref Rule::FirstFillSetForCYK).
  Generates map<pair<const
  NonTerminal*,const NonTerminal*>, unordered_set<const NonTerminal*>>. Uses for
  making pairs finding key of rules [A->BC]. Fills map with (<B,C>, [A]), where
  <B,C> is pair and adds A to unordered set. (@ref Rule::FirstFillSetForCYK).
  Used Saucekova CYK implementation pseudo code.
  Creates vector<vector<unordered_set<const NonTerminal*>>> for T from
  implementation. Iterates through T to generate possible word start non
  terminals. If possible word start non terminals contains grammar
  start_non_terminal word is from  grammar language.

  @author Petar Toshev
*/
bool Grammar::CYK(const string& word_string) const {
  vector_grammar_symbol_type word_symbols;
  // move string to grammar symbols
  {
    string new_word_to_skip_everything = start_non_terminal->Serialize();
    Rule rule(new_word_to_skip_everything, terminals, non_terminals);
    rule.IsValidParse(word_symbols, word_string, terminals, non_terminals);
    if (!rule.IsValid()) {
      std::cout << rule.GetError() << std::endl;
      return false;
    }
  }

  const us word_len = word_symbols.size();
  map_for_CYK_first_fill_word word_map;
  // move word symbols to map
  {
    for (us i = 0; i < word_len; i++) {
      const Terminal* symbol = (const Terminal*)word_symbols[i];
      // check if NonTerminal
      if (!symbol->IsTerminal()) {
        std::cout << "Cannot check word with NonTerminal "
                  << ((const NonTerminal*)symbol)->Serialize() << std::endl;
        return false;
      }
      map_for_CYK_first_fill_word::iterator it = word_map.find(symbol);
      if (it == word_map.end()) {
        CYK_vector_of_terminal_indexed new_vector_of_indexes;
        word_map.emplace(symbol, new_vector_of_indexes);
        it = word_map.find(symbol);
      }
      it->second.push_back(i);
    }
  }

  CYK_vector_of_vector_of_unordered_sets T;
  // fill vector with sets
  {
    unordered_set_const_non_terminal_ptr set_to_put;
    CYK_vector_of_unordered_sets vector_unordered_sets;
    for (us i = 0; i < word_len; i++)
      vector_unordered_sets.push_back(set_to_put);
    for (us i = 0; i < word_len; i++) T.push_back(vector_unordered_sets);
  }

  map_for_CYK_non_terminals_relations non_terminals_relations;
  // FirstFillSetForCYK T
  for (const Rule& rule : rules)
    rule.FirstFillSetForCYK(T, non_terminals_relations, word_map);

  /*
  // print cyk table
  if (false) {
    for (CYK_vector_of_unordered_sets& v : T) {
      for (unordered_set_const_non_terminal_ptr& s : v) {
        for (const NonTerminal* nt : s)
          std::cout << nt->Serialize() << std::endl;
        std::cout << "---\n";
      }
      std::cout << "\n\n===\n";
    }
    std::cout << "\n\nEND\n\n";
  }
  // print relations
  if (false) {
    map_for_CYK_non_terminals_relations::const_iterator it =
        non_terminals_relations.begin();
    while (it != non_terminals_relations.end()) {
      std::cout << "(" << it->first.first->Serialize() << ",\t"
                << it->first.second->Serialize() << ")\t->";
      for (const NonTerminal* nt : it->second)
        std::cout << '\t' << nt->Serialize();
      std::cout << std::endl;
      it++;
    }
  }
  */

  // CYK (Saucekova implemantation pseudo code used and corrected)
  for (us j = 1; j < word_len; j++) {
    for (us i = 0; i < word_len - j; i++) {
      unordered_set_const_non_terminal_ptr& set_T_i_j = T[i][j];
      for (us k = 0; k < j; k++) {
        unordered_set_const_non_terminal_ptr& set_T_i_k = T[i][k];
        unordered_set_const_non_terminal_ptr& set_T_ik_jk =
            T[i + k + 1][j - k - 1];
        if (set_T_ik_jk.size() == 0 || set_T_i_k.size() == 0) continue;
        for (const NonTerminal* B : set_T_i_k) {
          for (const NonTerminal* C : set_T_ik_jk) {
            const const_non_terminal_ptr_pair rule = {B, C};
            map_for_CYK_non_terminals_relations::const_iterator it =
                non_terminals_relations.find(rule);
            if (it != non_terminals_relations.end()) {
              const unordered_set_const_non_terminal_ptr& parent_set =
                  it->second;
              for (const NonTerminal* parent_non_terminal : parent_set)
                set_T_i_j.emplace(parent_non_terminal);
            }
          }
        }
      }
    }
  }

  // result calculating
  return T[0][word_len - 1].count(start_non_terminal) > 0;
}

/*!
  @brief Check if grammar language is empty

  @return Is language empty or not.

  Check if language of grammar is empty or is not.

  @author Petar Toshev
*/
bool Grammar::IsEmpty() const {
  unordered_set_for_grammar_symbols marked;
  // fill with terminals
  for (const Terminal* t : terminals) marked.emplace(t);

  bool has_changed = true;
  while (has_changed) {
    has_changed = false;
    for (const Rule& r : rules)
      has_changed =
          has_changed || r.HasToAddKeyToMarkedForIsEmptyGrammarLanguage(marked);
  }

  return marked.count(start_non_terminal) == 0;
}

/*!
  @brief Check if grammar is in Chomsky format.

  @return (bool) Is grammar in chomsky or false.

  @author Petar Toshev
*/
bool Grammar::IsInChomsky() const {
  bool res = true;
  for (const Rule& r : rules) {
    res = res && r.IsInChomsky(start_non_terminal);
    if (!res) break;
  }
  return res;
}

/*!
  @brief Creates iter (Start of Klini) on grammar.

  @return (bool) Is successfully creted iter.

  @author Petar Toshev
*/
bool Grammar::Iter() {
  const NonTerminal* new_start =
      CreateAndInsertNewNonTerminalWithFirstFreeNumberViaLetter(
          start_non_terminal->GetLetter());
  stringstream rule;
  string new_start_string = new_start->Serialize();
  const char* separator = " | ";
  rule << new_start_string << " -> " << new_start_string << new_start_string
       << separator << epsilon->Serialize() << separator
       << start_non_terminal->Serialize();
  if (!AddRule(rule.str())) return false;
  start_non_terminal = new_start;
  return true;
}

std::string Grammar::Print() const {
  stringstream s;
  SerializeOrPrint(s, false);
  return s.str();
}

/*!
  @brief Remove rule by id (in list)

  @param Rule id
  @return (bool) true, if rule has been removed, false
  if there is no such rule with this id

  @author Petar Toshev
*/
bool Grammar::RemoveRule(const us rule_id) {
  if (rules.size() > rule_id) {
    rules.erase(rules.begin() + rule_id);
    return true;
  }
  return false;
}

std::string Grammar::Serialize() const {
  stringstream s;
  SerializeOrPrint(s, true);
  return s.str();
}

/*!
  @brief Serializes or ptints grammar

  @param (ostringstream& result) where grammar to be serialized or printed
  @param (bool serialize) Is serialization
  @return (void)

  Serializes or prints grammar.
  If serialize -> prints all terminals,
  non_terminals, start terminal, epsilon and rules (Ready to be read and
  parsed).
  Else print -> prints all rules numbered;

  @author Petar Toshev
*/
void Grammar::SerializeOrPrint(stringstream& result, bool serialize) const {
  const char separator = ' ';
  const char new_line = '\n';

  if (serialize) {
    // non_terminals printing
    {
      us non_terminals_size = non_terminals.size();
      for (us i = 0; i < non_terminals_size; i++) {
        if (i) result << separator;
        result << non_terminals[i]->Serialize();
      }
      result << new_line;
    }

    // terminals printing
    {
      bool printing_separator = false;
      for (const Terminal* terminal : terminals) {
        if (!terminal->IsEpsilon()) {
          if (printing_separator) result << separator;
          result << terminal->Serialize();
          printing_separator = true;
        }
      }
      result << new_line;
    }

    // start terminal printing
    result << start_non_terminal->Serialize();
    result << new_line;

    // epsilon printing
    result << epsilon->Serialize();
    result << new_line;
  }

  // print rules
  us rules_size = rules.size();
  us rules_size_number_of_digits = log10(rules_size) + 1;
  const char rules_id_prefix = ' ';
  string id_prefixer = "";
  for (us i = 0; i < rules_size_number_of_digits; i++)
    id_prefixer += rules_id_prefix;
  us id_prefixer_scale = 1;

  for (us i = 0; i < rules_size; i++) {
    // we need i as id
    if (!serialize) {
      if (i % id_prefixer_scale == 0) {
        id_prefixer.resize(id_prefixer.length() - 1);
        id_prefixer_scale *= 10;
      }
      result << id_prefixer + std::to_string(i) + ": ";
    }
    result << rules[i].Serialize();
    result << new_line;
  }
}

/*!
  @brief Union this grammar with other grammar

  @param (Grammar&) grammar2 grammar to be unioned with
  @return void

  @warning Can throw error via Validate interface

  Uses @ref Grammar::ConcatenateOrUnion(Grammar& grammar2, const char separator)
*/
void Grammar::UnionWith(Grammar& grammar2) {
  ConcatenateOrUnion(grammar2, '|');
}
