#ifndef OOP_AUTOMATA_SRC_Serializable_H_
#define OOP_AUTOMATA_SRC_Serializable_H_

#include <string>

class Serializable {
 public:
  virtual std::string Serialize() const = 0;
};

#endif  // !OOP_AUTOMATA_SRC_Serializable_H_