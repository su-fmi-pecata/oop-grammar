#ifndef OOP_AUTOMATA_SRC_NON_TERMINAL_MANAGER_H_
#define OOP_AUTOMATA_SRC_NON_TERMINAL_MANAGER_H_

#include <fstream>
#include <map>
#include <vector>

#include "NonTerminal.h"

class NonTerminalManager {
  using ifstream = std::ifstream;
  using string = std::string;
  using us = unsigned short int;

  using non_terminals_first_free_map_type = std::map<const char, us>;
  using vector_non_terminal_type = std::vector<NonTerminal*>;

 public:
  const NonTerminal* CreateAndInsertNewNonTerminalWithFirstFreeNumberViaLetter(
      const char& letter);
#ifdef _DEBUG
  vector_non_terminal_type& GET_NON_TERMINALS() { return non_terminals; }
#endif  // DEBUG

 protected:
  void AddNewNonTerminalToMapWithFreeNumbers(const NonTerminal& nt);
  vector_non_terminal_type non_terminals;
  non_terminals_first_free_map_type non_terminals_first_free_map;
};

#endif  // OOP_AUTOMATA_SRC_NON_TERMINAL_MANAGER_H_