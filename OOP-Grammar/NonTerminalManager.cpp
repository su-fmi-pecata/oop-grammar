#include "NonTerminalManager.h"

#include <algorithm>

void NonTerminalManager::AddNewNonTerminalToMapWithFreeNumbers(
    const NonTerminal& nt) {
  const char& letter = nt.GetLetter();
  const us next_is_free = 1;
  const us& number = nt.GetNumber() + next_is_free;
  non_terminals_first_free_map_type::iterator it =
      non_terminals_first_free_map.find(letter);
  if (it == non_terminals_first_free_map.end())
    non_terminals_first_free_map.insert({letter, number});
  else
    it->second = std::max((int)it->second, (int)number);
}

/*!
  @brief Creates NonTerminal in heap via letter and first not
  used number

  @param (const char&) letter to be created with;
  @return ptr to NonTerminal in heap.

  Creates NonTerminal in heap via letter and first not
  used number and add to vector of non_terminals.

  @author Petar Toshev
*/
const NonTerminal*
NonTerminalManager::CreateAndInsertNewNonTerminalWithFirstFreeNumberViaLetter(
    const char& letter) {
  const us default_number = 1;
  NonTerminal* nt = new NonTerminal(&letter);
  non_terminals_first_free_map_type::iterator it =
      non_terminals_first_free_map.find(letter);
  if (it == non_terminals_first_free_map.end()) {
    non_terminals_first_free_map.insert({letter, default_number});
  } else {
    nt->SetNumber(it->second++);
  }
  non_terminals.push_back(nt);
  return nt;
}
