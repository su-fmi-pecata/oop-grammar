/*!
  @brief Abstract class for grammar symbols

  @author Petar Toshev
*/

#ifndef OOP_AUTOMATA_SRC_GRAMMAR_SYMBOL_H_
#define OOP_AUTOMATA_SRC_GRAMMAR_SYMBOL_H_

#include "Serializable.h"

class GrammarSymbol : public Serializable {
  using string = std::string;

 public:
  GrammarSymbol(const char& letter) { this->letter = letter; }
  GrammarSymbol(const GrammarSymbol& terminal_to_copy) {
    this->letter = terminal_to_copy.letter;
  }
  virtual bool IsTerminal() const = 0;
  virtual bool IsEpsilon() const = 0;

  const char& GetLetter() const { return letter; }

  virtual string Serialize() const {
    string c;
    c += letter;
    return c;
  }

  bool operator<(const GrammarSymbol& b) const { return letter < b.letter; }
  bool operator>(const GrammarSymbol& b) const { return letter > b.letter; }
  bool operator==(const GrammarSymbol& b) const { return letter == b.letter; }

#ifdef _DEBUG
  void SET_LETTER(char c) { letter = c; }
#endif  // DEBUG

 protected:
  char letter;
};

#endif  // !OOP_AUTOMATA_SRC_GRAMMAR_SYMBOL_H_
