#ifndef OOP_AUTOMATA_SRC_GRAMMAR_H_
#define OOP_AUTOMATA_SRC_GRAMMAR_H_

#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "Config.h"
#include "EpsilonTerminal.h"
#include "Generic.h"
#include "GrammarSymbol.h"
#include "NonTerminal.h"
#include "NonTerminalManager.h"
#include "Rule.h"
#include "Serializable.h"
#include "Terminal.h"
#include "Validate.h"

class Grammar : public Generic,
                public Serializable,
                public Validate,
                public NonTerminalManager {
  using ifstream = std::ifstream;
  using stringstream = std::stringstream;
  using string = std::string;
  using us = unsigned short int;

  using unordered_set_const_non_terminal_ptr =
      std::unordered_set<const NonTerminal*>;
  using const_non_terminal_ptr_pair =
      std::pair<const NonTerminal*, const NonTerminal*>;
  using CYK_vector_of_terminal_indexed = std::vector<us>;

  using changing_old_to_new_grammar_symbol_ptr_map_type =
      std::map<const GrammarSymbol*, GrammarSymbol*>;
  using CYK_vector_of_unordered_sets =
      std::vector<unordered_set_const_non_terminal_ptr>;
  using CYK_vector_of_vector_of_unordered_sets =
      std::vector<CYK_vector_of_unordered_sets>;
  using map_for_CYK_first_fill_word =
      std::map<const Terminal*, CYK_vector_of_terminal_indexed>;
  using map_for_CYK_non_terminals_relations =
      std::map<const const_non_terminal_ptr_pair,
               unordered_set_const_non_terminal_ptr>;
  using map_for_transfering_to_chomsky_remove_long_rules_type =
      std::map<const string, const NonTerminal*>;
  using map_for_transfering_to_chomsky_remove_combined_rules_type =
      std::map<const Terminal, const NonTerminal*>;
  using map_for_transfering_to_chomsky_remove_renaming_rules_type =
      std::unordered_map<const NonTerminal*,
                         unordered_set_const_non_terminal_ptr>;
  using non_terminals_first_free_map_type = std::map<const char, us>;
  using rule_vector_t = std::vector<Rule>;
  using unordered_set_for_epsilon_closing =
      std::unordered_set<const NonTerminal*>;
  using unordered_set_for_grammar_symbols =
      std::unordered_set<const GrammarSymbol*>;
  using vector_grammar_symbol_type = std::vector<const GrammarSymbol*>;
  using vector_terminal_type = std::vector<Terminal*>;
  using vector_non_terminal_type = std::vector<NonTerminal*>;

 public:
  static constexpr char EPSILON = '#';
  static constexpr char DEFAULT_TERMINALS[] = {
      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
      'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
      'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
  static constexpr char DEFAULT_NON_TERMINALS[] = {
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
      'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
  static constexpr char DEFAULT_START_NON_TERMINAL = 'S';

  /*Grammar(Config& config, rule_vector_t& rules, const us start_non_terminal,
          const char* non_terminals = DEFAULT_NON_TERMINALS,
          const char* terminals = DEFAULT_TERMINALS);*/
  Grammar(Config& config, ifstream& infile);
  Grammar(Grammar& grammar_to_copy);
  ~Grammar();

  bool AddRule(const string& rule);
  void ConcatenateWith(Grammar& grammar2);
  bool CYK(const string& word) const;
  bool IsEmpty() const;
  bool IsInChomsky() const;
  bool Iter();
  string Print() const;
  bool RemoveRule(const us);
  virtual string Serialize() const;
  void TransferToChomsky();
  void UnionWith(Grammar& grammar2);

#ifdef _DEBUG
  vector_terminal_type& GET_TERMINALS() { return terminals; }
#endif  // DEBUG

 private:
  vector_terminal_type terminals;
  rule_vector_t rules;
  const NonTerminal* start_non_terminal;
  const EpsilonTerminal* epsilon;

  NonTerminal* CombineWith(Grammar& grammar_to_combine_with);
  void ConcatenateOrUnion(Grammar& grammar2, const char separator);
  void SerializeOrPrint(stringstream& result, bool serialize = false) const;

  // functions for Chomsky transfer
  void RemoveLongRules();
  void RemoveEpsilonRules();
  void RemoveRenamingRules();
  void RemoveCombinedRules();

  // TODO: Petar Toshev (03.06.2019) [Map with terminals and non_terminals]
  // Warning: NonTerminalManager was not created when this todo was created
  // Change vectors of terminals and non_terminals to map or set with
  // terminals and ptr to it in heap

  /*
  template <class T>
  bool FillVectorWithTerminalsOrNonTerminals(ifstream& infile,
                                             std::vector<T>& where_to_push,
                                             const char* default_values) {
    string line;
    if (std::getline(infile, line) && line.length()) {
      const us skip_space = 1;
      const char* arr = line.c_str();
      while (*arr) {
        us len = 0;
        while (arr[len] && arr[len] != ' ') len++;
        where_to_push.push_back(T(arr, len));
        arr += len + (arr[len] ? skip_space : 0);
      }
    } else {
      us len = (sizeof(default_values) / sizeof(*default_values));
      for (us i = 0; i < len; i++)
        where_to_push.push_back(T(default_values + i, 0));
    }

    std::unordered_set<T> values_set;
    for (const T& value : where_to_push) values_set.emplace(value);

    return values_set.size() == where_to_push.size();
  }
  */
};

#endif  // OOP_AUTOMATA_SRC_GRAMMAR_H_
