#ifndef OOP_AUTOMATA_SRC_GENERIC_H_
#define OOP_AUTOMATA_SRC_GENERIC_H_

#include <string>

#include "Config.h"

class Generic {
  using us = unsigned short;

 public:
  Generic(Config& config, us _id) : config(&config), id(_id) {}
  Generic(Generic& generic) : config(generic.config), id(generic.id) {}
  Generic& operator=(Generic& generic) {
    id = generic.id;
    config = generic.config;
    return *this;
  }

  us GetId() { return id; }

 protected:
  Config* GetConfig() { return config; }

 private:
  us id;
  Config* config;
};

#endif  // OOP_AUTOMATA_SRC_GENERIC_H_
