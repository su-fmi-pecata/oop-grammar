var class_validate =
[
    [ "string", "class_validate.html#a775be6ccb03a2cba1719ffe3342d7225", null ],
    [ "us", "class_validate.html#a5e1811ff568970cd0e85dea824e2973d", null ],
    [ "~Validate", "class_validate.html#ab4dd113071ff6e8b49642f1416f57335", null ],
    [ "GetError", "class_validate.html#aa9dbf1349eeb3391a279ee8ac248f715", null ],
    [ "IsValid", "class_validate.html#a719d83eb51b4ac14e29a27fb3fd9fc85", null ],
    [ "SetError", "class_validate.html#a4dc9a0bbbb4d64fce41fa6d13805ef45", null ],
    [ "SetErrorWithCutomLeadingString", "class_validate.html#abfc7bb1538276dcd759ee1af4141fe1a", null ],
    [ "error", "class_validate.html#ab40ff0a639fcfe0dfdddee22fedfca5b", null ]
];